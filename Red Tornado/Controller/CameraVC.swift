//
//  ViewController.swift
//  Red Tornado
//
//  Created by Rajbir Singh on 2019-04-21.
//  Copyright © 2019 Shienh. All rights reserved.
//

import UIKit
import AVFoundation
import CoreML
import Vision

enum FlashState{
    case off
    case on
}

class CameraVC: UIViewController {
    
    var captureSession: AVCaptureSession!
    var cameraOutput: AVCapturePhotoOutput!
    var previewLayer: AVCaptureVideoPreviewLayer!
    
    var photoData : Data?
    
    var flashControlState: FlashState = .off

    @IBOutlet weak var cameraView: UIView!
    @IBOutlet weak var capturedImageView: RoundedShadowImageView!
    @IBOutlet weak var flashButton: RoundedShadowButton!
    @IBOutlet weak var identificationLabel: UILabel!
    @IBOutlet weak var confidenceLabel: UILabel!
    @IBOutlet weak var roundedLabelView: RoundedShadowView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.view.sendSubviewToBack(cameraView)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        previewLayer.frame = cameraView.bounds
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(didTapCameraView))
        tap.numberOfTapsRequired = 1
        
        captureSession = AVCaptureSession()
        captureSession.sessionPreset = AVCaptureSession.Preset.hd1920x1080
        
        let backCamera = AVCaptureDevice.default(for: AVMediaType.video)
        
        do{
            let input = try AVCaptureDeviceInput(device: backCamera!)
            if captureSession.canAddInput(input) == true{
                captureSession.addInput(input)
            }
            
            cameraOutput = AVCapturePhotoOutput()
            
            if captureSession.canAddOutput(cameraOutput) == true{
                captureSession.addOutput(cameraOutput!)
                
                previewLayer = AVCaptureVideoPreviewLayer(session: captureSession!)
                previewLayer.videoGravity = AVLayerVideoGravity.resizeAspect
                previewLayer.connection?.videoOrientation = AVCaptureVideoOrientation.portrait
                
                cameraView.layer.addSublayer(previewLayer!)
                cameraView.addGestureRecognizer(tap)
                captureSession.startRunning()
            }
        }
        catch{
            debugPrint(error)
        }
    }

    @objc func didTapCameraView(sender: UITapGestureRecognizer){
        self.cameraView.isUserInteractionEnabled = false
        let settings = AVCapturePhotoSettings()
        settings.previewPhotoFormat = settings.embeddedThumbnailPhotoFormat
        
        if flashControlState == .off {
            settings.flashMode = .off
        }else{
            settings.flashMode = .on
        }
        
        cameraOutput.capturePhoto(with: settings, delegate: self)
    }
    
    func resultsMethod(request: VNRequest, error: Error?){
        //Handle Changing the label text
        guard let results = request.results as? [VNClassificationObservation] else {return}
        for classification in results{
            if(classification.confidence<0.5){
                self.identificationLabel.text="Unable to understand"
                self.confidenceLabel.text = ""
                break
            }else{
                self.identificationLabel.text = classification.identifier
                debugPrint(classification.identifier)
                self.confidenceLabel.text = "Confidence: \(Int(classification.confidence*100))%"
                debugPrint("Confidence: \(Int(classification.confidence*100))%")
                let Tornado = "I am \(Int(classification.confidence*100))% confident that this is "+classification.identifier
                let alert = UIAlertController(title: "Alert", message: Tornado, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    switch action.style{
                    case .default:
                        print("default")
                        
                    case .cancel:
                        print("cancel")
                        
                    case .destructive:
                        print("destructive")
                    }}))
                self.present(alert, animated: true, completion: nil)
                break
            }
        }
    }
    
    @IBAction func flashButtonPressed(_ sender: Any) {
        switch flashControlState {
        case .off:
            flashButton.setTitle("Flash ON", for: .normal)
            flashControlState = .on
        case .on:
            flashButton.setTitle("Flash OFF", for: .normal)
            flashControlState = .off
        }
    }
}



extension CameraVC: AVCapturePhotoCaptureDelegate{
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        if let error = error{
            debugPrint(error)
        }else{
            photoData = photo.fileDataRepresentation()
            
            do{
                let brain = try VNCoreMLModel(for: SqueezeNet().model)
                let thought = VNCoreMLRequest(model: brain, completionHandler: resultsMethod)
                let handler = VNImageRequestHandler(data: photoData!)
                try handler.perform([thought])
                
            }catch{
                //Handle Errors
                debugPrint(error)
            }
            let image = UIImage(data: photoData!)
            self.capturedImageView.image = image
        }
    }
}

